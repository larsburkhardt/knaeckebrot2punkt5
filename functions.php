<?php

require_once('lib/styles-and-scripts.php');     // Enqueue Styles and scripts
require_once('lib/the-menus.php');              // The Menus
require_once('lib/theme-support.php');          // Theme Support
require_once('lib/the-sidebars.php');           // The Sidebars
require_once('lib/dashboard-widget-lb.php');  // Dashboard-Widget



function knaeckebrot_content_nav( $html_id ) {
	global $wp_query;

	if ( $wp_query->max_num_pages > 1 ) : ?>
			<nav id="<?php echo esc_attr( $html_id ); ?>" class="other-posts">
				<h3 class="assistive-text">Beitrags-Navigation</h3>
				<div class="older-posts"><?php next_posts_link( __('&Auml;ltere Eintr&auml;ge') );?></div> 
				<div class="newer-posts"><?php previous_posts_link( __('Neuere Eintr&auml;ge') ); ?></div>
			</nav>
	<?php endif;
}


/*  EXCERPT 
    Usage:
    
    <?php echo excerpt(100); ?>
*/

function excerpt($limit) {
    $excerpt = explode(' ', get_the_excerpt(), $limit);
    if (count($excerpt)>=$limit) {
    array_pop($excerpt);
    $excerpt = implode(" ",$excerpt).'...';
    } else {
    $excerpt = implode(" ",$excerpt);
    } 
    $excerpt = preg_replace('`\[[^\]]*\]`','',$excerpt);
    return $excerpt;
}

// REMOVE WIDGET TITLE IF IT BEGINS WITH EXCLAMATION POINT
function knaeckebrot_remove_widget_title( $widget_title ) {
    if ( substr ( $widget_title, 0, 1 ) == '!' )
        return;
    else
        return ( $widget_title );
}
add_filter( 'widget_title', 'knaeckebrot_remove_widget_title' );

