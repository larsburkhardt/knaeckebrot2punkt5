<?php get_header(); ?>

<?php 
    $sidebar = is_active_sidebar('sidebar-1');
?>



 <div class="row">
   <main class="col-xs-12 col-md-<?php echo $sidebar ? '8' : '12'; ?>" id="main" role="main">
 
	<?php get_template_part('loop');?>


    <!-- Content-Widgets -->
    <?php get_template_part('template-parts/sidebars-bottom');?>

 
    </main><!-- main -->
 

	<?php if($sidebar) : ?>
		<?php get_sidebar(); ?>
	<?php endif; ?>

</div>
<?php get_footer(); ?>