<?php get_header(); ?>

<?php 
    $sidebar = is_active_sidebar('sidebar-1');
?>


	<div class="row">
        <main class="col-xs-12 col-md-<?php echo $sidebar ? '8' : '12'; ?>" id="main" role="main">
			<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
			<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
					<nav id="nav-single">
						<h3 class="assistive-text"><?php _e( 'Post navigation', 'knaeckebrot' ); ?></h3>
						<span class="nav-single-previous"><?php previous_post_link( '%link', __( '<span class="meta-nav"></span> Zurück', 'knaeckebrot' ) ); ?></span>
						<span class="nav-single-next"><?php next_post_link( '%link', __( 'Weiter <span class="meta-nav"></span>', 'knaeckebrot' ) ); ?></span>
					</nav><!-- #nav-single -->

				<header>
					<h1 class="article-h1"><a href="<?php the_permalink() ?>" rel="bookmark"><?php the_title(); ?></a></h1>
					<p class="entry-meta">
						Veröffentlicht am <?php the_time('j. F Y'); ?>
					</p>
				</header>
				
				<?php the_post_thumbnail(); ?>

				<section class="entry">
					<?php the_content(); ?>
				</section>
				
				<footer class="article-footer">
					<p class="posted-in">Gepostet in <?php the_category(', '); ?></p>
					<?php the_tags( '<p class="post-tags">Tags: ', ', ', '</p>'); ?>
				</footer>
			</article>
			<?php endwhile; ?>
			<nav class="other-posts">
				<h3 class="assistive-text">Beitrags-Navigation</h3>
				<div class="older-posts"><?php next_posts_link('&Auml;ltere Eintr&auml;ge') ?></div> 
				<div class="newer-posts"><?php previous_posts_link('Neuere Eintr&auml;ge') ?></div>
			</nav>
 
			<?php endif; ?>
			

		</main>

	<?php if($sidebar) : ?>
		<?php get_sidebar(); ?>
	<?php endif; ?>

		
	</div>
		<?php get_footer(); ?>