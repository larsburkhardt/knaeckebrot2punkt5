<form class="header__search-form" role="search" method="get" action="<?php echo esc_url( home_url( '/' ) ); ?>">
    <label class="header__search-form__label">
        <span class="screen-reader-text"><?php echo esc_html_x( 'Search for:', 'label', 'knaeckebrot' ) ?></span>
        <input class="header__search-form__input" type="search"  placeholder="<?php echo esc_attr_x( 'Search &hellip;', 'placeholder', 'knaeckebrot' ) ?>" value="<?php echo esc_attr(get_search_query()) ?>" name="s" />
    </label>
    <button class="header__search-form__button" type="submit"><span class="screen-reader-text"><?php echo esc_html_x( 'Search', 'submit button', 'knaeckebrot' ); ?></button>
</form>