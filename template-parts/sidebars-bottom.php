    <?php
        $sidebar_bottom_left = is_active_sidebar('sidebar-bottom-left');
        $sidebar_bottom_right = is_active_sidebar('sidebar-bottom-right');
    ?>
    <?php if( $sidebar_bottom_left || $sidebar_bottom_right) : ?>
    <div class="sidebar-bottom-wrapper">
        <div class="sidebar-bottom-container">
            <div class="row">
                <?php if($sidebar_bottom_left && !$sidebar_bottom_right) : ?>
                <aside class="sidebar-bottom-left col-xs-12" role="complementary">
                    <?php dynamic_sidebar('sidebar-bottom-left'); ?>
                </aside>
                <?php elseif(!$sidebar_bottom_left && $sidebar_bottom_right) : ?>
                <aside class="sidebar-bottom-right col-xs-12" role="complementary">
                    <?php dynamic_sidebar('sidebar-bottom-right'); ?>
                </aside>
                <?php else : ?>
                <aside class="sidebar-bottom-left col-xs-12 col-md-6" role="complementary">
                    <?php dynamic_sidebar('sidebar-bottom-left'); ?>
                </aside>
                <aside class="sidebar-bottom-right col-xs-12 col-md-6" role="complementary">
                    <?php dynamic_sidebar('sidebar-bottom-right'); ?>
                </aside>
                <?php endif; ?>
            </div>
        </div>
    </div>
    <?php endif; ?>
