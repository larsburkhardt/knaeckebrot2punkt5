<!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js">


<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<?php if ( is_singular() && pings_open() ) : ?>
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
	<?php endif; wp_head(); ?>

	</head>
<body <?php body_class(); ?>>
	<div id="wrapper" class="container-fluid">
		<header class="siteheader">
            <?php if(has_custom_logo()) :
                the_custom_logo(); 
                else : ?>
                <a class="header__blogname" href="<?php echo esc_url(home_url( '/' )); ?>">
                    <?php esc_html(bloginfo( 'name' )); ?>
                </a>
                <p class="blog-description"><?php bloginfo( 'description' ); ?></p>

            <?php endif; ?>

			<button id="toggle"><span>Menü</span></button>
			<nav class="navigation">
				<?php wp_nav_menu( array(
				'theme_location' => 'header-menu',
				'container' => '',
				'fallback_cb' => '__return_false'
				) ); ?>
			</nav>
		</header>
		
