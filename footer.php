		<footer class="sitefooter col-xs-12"  role="contentinfo">
				<?php wp_nav_menu( array(
				'theme_location' => 'footer-menu',
				'container' => 'nav',
				'container_class' => 'footermenu-container',
				'menu_id' => 'footermenu',
				'menu_class' => 'footermenu',
				'fallback_cb' => '__return_false'
				) ); ?>
			<div class="footer-pin-right"></div>
		</footer>
	</div> <!-- Ende Wrapper -->
	
	<?php wp_footer(); ?>
	
</body>
</html>