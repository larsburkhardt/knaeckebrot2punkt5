<?php

function knaeckebrot_menus() {
register_nav_menus( array(
'header-menu' => esc_html__( 'Header Menu', 'knaeckebrot' ),
'footer-menu' => esc_html__( 'Footer Menu', 'knaeckebrot'),
) );
}
add_action( 'init', 'knaeckebrot_menus' );