<?php 

/* Add widget support */
function knaeckebrot_widgets_init() {

if ( function_exists('register_sidebar') )
    register_sidebar(array(
		'name'          => esc_html__( 'Sidebar', 'knaeckebrot' ),
		'id'            => 'sidebar-1',
		'description'   => esc_html__( 'Widgets for the Sidebar.', 'knaeckebrot' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s col-xs-12 col-sm-6 col-md-12"><div class="aside-inner">',
		'after_widget'  => '</div></section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
    ));
    
if ( function_exists('register_sidebar') )
    register_sidebar(array(
		'name'          => esc_html__( 'Content Bottom Left', 'knaeckebrot' ),
		'id'            => 'sidebar-bottom-left',
		'description'   => esc_html__( 'Widgets for the bottom of the content, left side.', 'knaeckebrot' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s col-xs-12 col-sm-6">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
    ));
if ( function_exists('register_sidebar') )
    register_sidebar(array(
		'name'          => esc_html__( 'Content Bottom Right', 'knaeckebrot' ),
		'id'            => 'sidebar-bottom-right',
		'description'   => esc_html__( 'Widgets for the bottom of the content, right side', 'knaeckebrot' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s col-xs-12 col-sm-6">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
    ));

}
add_action('widgets_init', 'knaeckebrot_widgets_init');
