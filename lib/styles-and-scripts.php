<?php

function knaeckebrot_assets() {
    wp_enqueue_style('knaeckebrot-stylesheet', get_template_directory_uri() . '/dist/css/main.css', array(), '1.0.0', 'all');
    wp_enqueue_script( 'knaeckebrot-scripts', get_template_directory_uri() . '/dist/js/knaeckebrot.js', array('jquery'), '1.0.0', true );
}

add_action( 'wp_enqueue_scripts', 'knaeckebrot_assets' );


function knaeckebrot_admin_assets() {
    wp_enqueue_style( 'knaeckebrot-admin-stylesheet', get_template_directory_uri() . '/dist/css/admin.css', array(), '1.0.0', 'all' );
    wp_enqueue_script( 'knaeckebrot-admin-scripts', get_template_directory_uri() . '/dist/js/admin.js', array(), '1.0.0', true );
}

add_action( 'admin_enqueue_scripts', 'knaeckebrot_admin_assets' );

// Comment reply script
if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
    wp_enqueue_script( 'comment-reply' );
}