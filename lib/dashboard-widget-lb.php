<?php

/* Add Dashboard Widget */
/**
 * Add a widget to the dashboard.
 *
 * This function is hooked into the 'wp_dashboard_setup' action below.
 */
function knaeckebrot_add_dashboard_widgets() {
    wp_add_dashboard_widget(
        'knaeckebrot_dashboard_widget',                          // Widget slug.
        esc_html__( 'Support', 'knaeckebrot' ), // Title.
        'knaeckebrot_dashboard_widget_render'                    // Display function.
    );
}
add_action( 'wp_dashboard_setup', 'knaeckebrot_add_dashboard_widgets' );

/**
 * Create the function to output the content of our Dashboard Widget.
 */
function knaeckebrot_dashboard_widget_render() {
    // Display whatever you want to show.
    echo '
<p>Lars Burkhardt</p>
<table border="0" cellspacing="0">
    <tr valign="top">
        <td>Tel.</td>
        <td>0176 2317 2661</td>
    </tr>
    <tr valign="top">
        <td>Mail</td>
        <td>
            <a href="mailto:mail@larsburkhardt.de?subject=Support%20Website">mail@larsburkhardt.de</a>
        </td>
    </tr>
    <tr valign="top">
        <td>Web</td>
        <td>
            <a href="https://www.larsburkhardt.de">www.larsburkhardt.de</a>
            <br />
            <a href="https://design.larsburkhardt.de">design.larsburkhardt.de</a>
        </td>
    </tr>
</table>
<br>


<table border="0" cellspacing="0">
    <tr valign="top">
        <td width="60">XING</td>
        <td>
            <a href="https://xing.to/larsburkhardt">xing.to/larsburkhardt</a>
        </td>
    </tr>
    <tr valign="top">
        <td>LinkedIn</td>
        <td>
            <a href="https://www.linkedin.com/in/larsburkhardt1" >linkedin.com/in/larsburkhardt1</a>
        </td>
    </tr>
    <tr valign="top">
        <td>Twitter</td>
        <td>
            <a href="https://twitter.com/LarsBurkhardt">twitter.com/LarsBurkhardt</a>
        </td>
    </tr>
    <tr valign="top">
        <td>Facebook</td>
        <td>
            <a href="https://www.facebook.com/lars.burkhardt1" >facebook.com/lars.burkhardt1</a>
        </td>
    </tr>
    <tr valign="top">
        <td>Instagram</td>
        <td>
            <a href="https://www.instagram.com/design.larsburkhardt.de/">instagram.com/design.larsburkhardt.de/</a>
        </td>
    </tr>
</table>';
}

