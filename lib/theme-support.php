<?php

// Enable HTML5 Support and Gutenberg

function knaeckebrot_theme_support() {
	add_theme_support( 'title-tag' );           // Title in Tab
	add_theme_support( 'post-thumbnails' );     // Thumbnails in Posts
	add_theme_support( 'html5', array('search-form', 'comment-list', 'comment-form', 'gallery', 'caption') );     // HTML5 for specific elements
	add_theme_support( 'editor-styles' );           // Gutenberg
	add_editor_style('dist/css/editor.css'); // Gutenberg css
    add_theme_support( 'align-wide' );				// enable wide and full-size images
    add_theme_support( 'custom-logo', array(    
    'width' => 240,
    'height' => 120,
    'flex-width' => true,
    'flex-height' => true
    ) ); 


}
add_action( 'after_setup_theme', 'knaeckebrot_theme_support' );



/* Add custom thumbnail sizes */
if ( function_exists( 'add_image_size' ) ) {
    //add_image_size( 'custom-image-size', 500, 500, true );
}
