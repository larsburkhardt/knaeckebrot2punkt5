<?php
/*
* Template Name: Ohne Sidebar
*/
?>


<?php get_header(); ?>
 
   <main class="fullwidth col-xs-12" id="main" role="main">
 
	<?php get_template_part('loop');?>
 
   </main><!-- main -->
 
 
<?php get_footer(); ?>