# Knaeckebrot 2.5 - Basic Theme for Wordpress with Gulp and Sass

## Instructions
Move the folder "knaeckebrot2punkt5" to your theme folder in your Wordpress installation ("wp-content/themes/").
Theme should now appear in the theme section of Wordpress.

## Features

* Flexbox Grid by [flexboxgrid.com](http://www.flexboxgrid.com)
* Mobile-first
* Responsive Navigation with Toggle Button (Hamburger Menu)
* 2 Menu positions (header, widget area, footer)
* 2 Content widgets (if you don't need them, delete them in index.php at line 25 and remove the code in functions.php from line 50 to 70)

## Useage (Gulp and Sass)
For this version of the theme you need to have [node.js](https://nodejs.org/) and [Gulp](gulpjs.com) installed.
After that, open the Terminal/Console and run 
```
npm install
```
so that the dependencies will be installed.
When you are done, run
```
gulp dev 
```
and the Sass and JS will be watched and compiled permanently.

For Building use 
```
gulp build
```

### Gulp Features
* ES6 Support
* Image optimization 
* Sass compiling and bundling, auto-prefixing
* Bundling js files



## Preview
Preview of the template at [http://knaeckebrot2.larsburkhardt.de](http://knaeckebrot2.larsburkhardt.de)